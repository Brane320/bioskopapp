import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Link, Switch, Route } from 'react-router-dom';
import { Navbar, Nav, Container,Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from "./components/login/Login"
import Register from "./components/register/Register"
import {logout} from './services/auth'
import Filmovi from './components/filmovi/Filmovi';
import Film from './components/filmovi/Film';
import CreateFilm from './components/filmovi/CreateFilm';
import EditFilma from './components/filmovi/EditFilma';




class App extends React.Component {
  

  render() {
    return (
        
      <div>
      <Router>
        <Navbar expand bg="dark" variant="dark">
          <Navbar.Brand as={Link} to="/">
              Home
          </Navbar.Brand>
         
          
          <Nav>
            
          <Nav.Link as={Link} to="/filmovi">Prikaz svih filmova</Nav.Link>
          
            {window.localStorage['jwt'] ? 
                      <Button hidden onClick = {()=>logout()}>Logout</Button> :
                      <Nav.Link as={Link} to="/register">Register</Nav.Link>
                      }

            
           
            {window.localStorage['jwt'] ? 
                      <Button onClick = {()=>logout()}>Logout</Button> :
                      <Nav.Link as={Link} to="/login">Login</Nav.Link>
                      }
          </Nav>
        </Navbar>
          <Container style={{paddingTop:"25px"}}>
            <Switch>
              
              
            <Route exact path="/login" component={Login}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/filmovi" component={Filmovi}/>
            <Route exact path="/film/:id" component={Film}/>
            <Route exact path="/filmovi/create" component={CreateFilm}/>
            <Route exact path="/filmovi/edit/:id" component={EditFilma}/>

            
              
              
            </Switch>
          </Container>
        </Router>
        
      </div>
    )
}
};

ReactDOM.render(
    <App/>,
    document.querySelector('#root')
);