import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Table,Button} from "react-bootstrap";
import casino from '../../slike/casino.jpg'; 
import terminator from '../../slike/terminator.jpg';
import wolf from '../../slike/wolfofwallstreet.jpg';  



class Film extends React.Component {

    constructor(props){
        super(props);

        let film = {
            id : 0,
            naziv : "",
            reziser : "",
            glumci : "",
            zanrovi: "",
            trajanje : 0,
            distributer : "",
            zemljaPorekla : "",
            godinaProizvodnje : 0,
            opis : ""
        }

    this.state = { film : film};
}

componentDidMount(){
    this.getFilmById(this.props.match.params.id);
}

getFilmById(filmId) {
    TestAxios.get('/filmovi/' + filmId)
    .then(res => {
        // handle success
        console.log(res.data);
        let film = {
            id : res.data.id,
            naziv : res.data.naziv,
            reziser : res.data.reziser,
            glumci : res.data.glumci,
            zanrovi: res.data.zanrovi,
            trajanje : res.data.trajanje,
            distributer : res.data.distributer,
            zemljaPorekla : res.data.zemljaPorekla,
            godinaProizvodnje : res.data.godinaProizvodnje,
            opis : res.data.opis
        }
        this.setState({film : film});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Film nije dobijen')
     });
}


renderFilm() {
    
        return (
           <tr key={this.state.film.id}>
              <td>{this.state.film.naziv}</td>
              <td>{this.state.film.zanrovi}</td>
              <td>{this.state.film.distributer}</td>
              <td>{this.state.film.trajanje}</td>
              <td>{this.state.film.zemljaPorekla}</td>
              <td>{this.state.film.godinaProizvodnje}</td>
              <td>{this.state.film.reziser}</td>
              <td>{this.state.film.glumci}</td>
              <td>{this.state.film.opis}</td>
              

           
           </tr>
        )
     
}

goToEdit(filmId){
    this.props.history.push('/filmovi/edit/' + filmId);
}

delete(id) {
    TestAxios.delete('/filmovi/' + id)
    .then(res => {
        // handle success
        console.log(res);
        alert('Film uspesno obrisan');
        this.props.history.push('/filmovi');
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Problem sa brisanjem ');
     });
}
prikaziSliku(){
    if(this.state.film.naziv == "Casino"){
    return (
        <img src={casino} alt="casino" width="1110" height="300" />
    )
    } else if(this.state.film.naziv == "Terminator"){
        return (
            <img src={terminator} alt="terminator" width="1110" height="300" />
        )
    } else if(this.state.film.naziv == "The Wolf Of Wall Street"){
        return (
            <img src={wolf} alt="wallstreet" width="1110" height="300"/>
        )
    }
}

render() {
     
    return (
        <div>
              
       
        {this.prikaziSliku()}
        <Table striped style={{marginTop:5}}>
            <thead className="thead-dark">
                <tr>
                    <th>Naziv filma</th>
                    <th>Zanrovi</th>
                    <th>Distributer</th>
                    <th>Trajanje</th>
                    <th>Zemlja porekla</th>
                    <th>Godina proizvodnje</th>
                    <th>Reziser</th>
                    <th>Glumci</th>
                    <th>Opis(IMDB)</th>
                    
                
                    
                    
                    
                    
                    
                </tr>
            </thead>
            <tbody>
                {this.renderFilm()}
            </tbody>                  
        </Table>
        <Button style={{float:"right"}} variant="danger" onClick={()=>this.delete(this.state.film.id)}>Brisanje filma</Button>
        <Button style={{ float: "right" , marginRight : "10px" }}  onClick={()=>this.goToEdit(this.state.film.id)}>Edit filma</Button>
    </div>
  


);
}


}
export default Film;