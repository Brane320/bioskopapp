import React from 'react';
import TestAxios from '../../apis/TestAxios';
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
class CreateFilm  extends React.Component {
    constructor(props){
        super(props);

    let film = {
        naziv: "",
        reziser : "",
        glumci: "",
        zanrovi: "",
        trajanje: 0,
        distributer : "",
        zemljaPorekla : "",
        godinaProizvodnje : 0,
        opis : ""

    }

    this.state = {film: film};

}
create(e){
    let film = this.state.film;

    let filmDTO = {
        naziv: film.naziv,
        reziser : film.reziser,
        glumci: film.glumci,
        zanrovi: film.zanrovi,
        trajanje: film.trajanje,
        distributer : film.distributer,
        zemljaPorekla : film.zemljaPorekla,
        godinaProizvodnje : film.godinaProizvodnje,
        opis : film.opis

    }
    TestAxios.post('/filmovi', filmDTO)
    .then(res => {
        console.log(res);
        alert('Uspesno dodat film');
        this.props.history.push('/filmovi');
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesno kreiranje filma!');
    });
}

valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let film = this.state.film;
    film[name] = value;

    this.setState({ film : film});
  }

  render(){
    return (
        <>
        <Row>
        <Col></Col>
        <Col xs="14" sm="12" md="12">
        <Form>
            <Form.Label htmlFor="znaziv">Naziv</Form.Label>
            <Form.Control id="znaziv" name="naziv" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zReziser">Reziser</Form.Label>
            <Form.Control id="zReziser" name="reziser" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zGlumci">Glumci</Form.Label>
            <Form.Control id="zGlumci" name="glumci" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zZanrovi">Zanrovi</Form.Label>
            <Form.Control id="zZanrovi" name="zanrovi" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zTrajanje">Trajanje</Form.Label>
            <Form.Control type="number" id="zTrajanje" name="trajanje" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zDistributer">Distributer</Form.Label>
            <Form.Control id="zDistributer" name="distributer" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zPoreklo">Zemlja porekla</Form.Label>
            <Form.Control id="zPoreklo" name="zemljaPorekla" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zProizvodnja">Godina proizvodnje</Form.Label>
            <Form.Control type="number" id="zProizvodnja" name="godinaProizvodnje" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zOpis">Opis</Form.Label>
            <Form.Control id="zOpis" name="opis" onChange={(e)=>this.valueInputChanged(e)}/>

            
            
        

  
            <Button style={{ marginTop: "25px" }}onClick={(e) => this.create(e)}>Dodaj</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>


      
 
    </>

    

    )
}




}
export default CreateFilm;