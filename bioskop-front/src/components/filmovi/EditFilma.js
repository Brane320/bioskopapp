import React from 'react';
import TestAxios from '../../apis/TestAxios';
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
class EditFilma  extends React.Component {
    constructor(props){
        super(props);

    let film = {
        id : -1,
        naziv: "",
        reziser : "",
        glumci: "",
        zanrovi: "",
        trajanje: 0,
        distributer : "",
        zemljaPorekla : "",
        godinaProizvodnje : 0,
        opis : ""

    }

    this.state = {film: film};

}

componentDidMount(){
    this.getFilmById(this.props.match.params.id);
}

getFilmById(filmId) {
    TestAxios.get('/filmovi/' + filmId)
    .then(res => {
        // handle success
        console.log(res.data);
        let film = {
            id : res.data.id,
            naziv: res.data.naziv,
            reziser : res.data.reziser,
            glumci: res.data.glumci,
            zanrovi: res.data.zanrovi,
            trajanje: res.data.trajanje,
            distributer : res.data.distributer,
            zemljaPorekla : res.data.zemljaPorekla,
            godinaProizvodnje : res.data.godinaProizvodnje,
            opis : res.data.opis
    
        }
        this.setState({film : film});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Film nije dobijen')
     });
}

valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let film = this.state.film;
    film[name] = value;

    this.setState({ film : film });
  }

  edit(){
    let film = this.state.film;
    let filmDTO = {
        id : film.id,
        naziv: film.naziv,
        reziser : film.reziser,
        glumci: film.glumci,
        zanrovi: film.zanrovi,
        trajanje: film.trajanje,
        distributer : film.distributer,
        zemljaPorekla : film.zemljaPorekla,
        godinaProizvodnje : film.godinaProizvodnje,
        opis : film.opis
        
        


    }
    TestAxios.put('/filmovi/' + this.state.film.id, filmDTO)
    .then(res => {
        console.log(res);

        alert('Film uspesno izmenjen');
        this.props.history.push("/filmovi");
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesno editovanje zadatka!');
    });
}

render(){
    return (
        <>
        <Row>
        <Col></Col>
        <Col xs="14" sm="12" md="12">
        <Form>
            <Form.Label htmlFor="znaziv">Naziv</Form.Label>
            <Form.Control id="znaziv" name="naziv" value ={this.state.film.naziv} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zReziser">Reziser</Form.Label>
            <Form.Control id="zReziser" name="reziser" value={this.state.film.reziser} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zGlumci">Glumci</Form.Label>
            <Form.Control id="zGlumci" name="glumci" value={this.state.film.glumci}onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zZanrovi">Zanrovi</Form.Label>
            <Form.Control id="zZanrovi" name="zanrovi" value={this.state.film.zanrovi} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zTrajanje">Trajanje</Form.Label>
            <Form.Control type="number" id="zTrajanje" name="trajanje" value={this.state.film.trajanje} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zDistributer">Distributer</Form.Label>
            <Form.Control id="zDistributer" name="distributer" value={this.state.film.distributer} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zPoreklo">Zemlja porekla</Form.Label>
            <Form.Control id="zPoreklo" name="zemljaPorekla" value={this.state.film.zemljaPorekla} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zProizvodnja">Godina proizvodnje</Form.Label>
            <Form.Control type="number" id="zProizvodnja" name="godinaProizvodnje" value={this.state.film.godinaProizvodnje} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zOpis">Opis</Form.Label>
            <Form.Control id="zOpis"  name="opis" value={this.state.film.opis} onChange={(e)=>this.valueInputChanged(e)}/>

            
            
        

  
            <Button style={{ marginTop: "25px" }}onClick={() => this.edit()}>Izmeni film</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>


      
 
    </>

    

    )
}

}

export default EditFilma;