import React from 'react';
import {Table, Button, Form,Col,InputGroup,Row} from 'react-bootstrap'
import TestAxios from '../../apis/TestAxios';

class Filmovi extends React.Component {

    constructor(props) {
        super(props);

        

        this.state = { filmovi: [], brojStranice : 0,totalPages : 0}
    }
    componentDidMount(){
        this.getFilmove(0);
    
    }

    getFilmove(brojStranice) {
        let config = {
            params: {
              brojStranice: this.state.brojStranice,
              brojStranice : brojStranice
            },
          }
        TestAxios.get('/filmovi', config)
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({brojStranice : brojStranice, filmovi : res.data,  totalPages : res.headers["total-pages"]});
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Greska pri ocitavanju filmova');
            });
    }
    
    

    renderFilmove() {
        return this.state.filmovi.map((film) => {
            return (
               <tr key={film.id}>
                  <td><a href={'/#/film/' + film.id}>{film.naziv}</a></td>
                  <td>{film.zanrovi}</td>
                  <td>{film.distributer}</td>
                  <td>{film.trajanje}</td>
                  <td>{film.zemljaPorekla}</td>
                  <td>{film.godinaProizvodnje}</td>
                  
                  
    
               
               </tr>
            )
         })
    }
    goToCreate(){
        this.props.history.push('/filmovi/create');
    }

    
    render() {
     
        return (
            <div>
                  
            <div>
                        
            <br/>
            <div style={{float:"left"}}>  
            <Button onClick={(e) => this.goToCreate()}>Kreiraj novi film</Button>
            </div>
            <div style={{float:"right"}}><Button disabled={this.state.brojStranice==0} onClick={()=>this.getFilmove(this.state.brojStranice -1)}>Prethodna stranica</Button>
          
          <Button disabled={this.state.brojStranice==this.state.totalPages-1} onClick={()=>this.getFilmove(this.state.brojStranice+1)}>Sledeća stranica</Button>
      </div>
            
            <Table striped style={{marginTop:5}}>
                <thead className="thead-dark">
                    <tr>
                        <th>Naziv filma</th>
                        <th>Zanrovi</th>
                        <th>Distributer</th>
                        <th>Trajanje</th>
                        <th>Zemlja porekla</th>
                        <th>Godina proizvodnje</th>
                        
                        
                        
                        
                        
                        
                        
                        
                    </tr>
                </thead>
                <tbody>
                    {this.renderFilmove()}
                </tbody>                  
            </Table>
            
        </div>
      
    
    </div>
    
    );
    }
    

}
export default Filmovi;