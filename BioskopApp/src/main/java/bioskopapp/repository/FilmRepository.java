package bioskopapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bioskopapp.model.Film;

@Repository
public interface FilmRepository extends JpaRepository<Film,Long>{

}
