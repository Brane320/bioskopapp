package bioskopapp.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bioskopapp.model.Film;
import bioskopapp.service.FilmService;
import bioskopapp.support.FilmDTOToFilm;
import bioskopapp.support.FilmToFilmDTO;
import bioskopapp.web.dto.FilmDTO;


@RestController
@RequestMapping(value = "/api/filmovi", produces = MediaType.APPLICATION_JSON_VALUE)
public class FilmController {
	
	@Autowired
	private FilmService filmService;
	
	@Autowired
	private FilmToFilmDTO toFilmDTO;
	
	@Autowired
	private FilmDTOToFilm toFilm;
	
	@GetMapping									
	public ResponseEntity<List<FilmDTO>> getAll(@RequestParam(defaultValue="0") int brojStranice){
		Page <Film> svi = filmService.findAll(brojStranice);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Total-Pages", svi.getTotalPages() + "");
		return new ResponseEntity<>(toFilmDTO.convert(svi.getContent()),headers,HttpStatus.OK);
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<FilmDTO> get(@PathVariable Long id){
		Optional<Film> film = filmService.findById(id);
		
		if(film.isPresent()) {
			return new ResponseEntity<>(toFilmDTO.convert(film.get()), HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDTO> create(@Valid @RequestBody FilmDTO filmDTO){
	   Film film = toFilm.convert(filmDTO);
       
	   
	   Film sacuvan = filmService.save(film);
       
      

        return new ResponseEntity<>(toFilmDTO.convert(sacuvan), HttpStatus.CREATED);
    }
	
	@PutMapping(value= "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FilmDTO> update(@PathVariable Long id, @Valid @RequestBody FilmDTO filmDTO){
		if(!id.equals(filmDTO.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Film film = toFilm.convert(filmDTO);
		
		
		
		Film izmenjen = filmService.update(film);
		
		return new ResponseEntity<>(toFilmDTO.convert(izmenjen),HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<FilmDTO> delete(@PathVariable Long id){
		Film obrisan = filmService.delete(id);
		
		if(obrisan != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	


}
