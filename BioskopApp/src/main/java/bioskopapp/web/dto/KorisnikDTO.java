package bioskopapp.web.dto;

import javax.validation.constraints.NotBlank;

public class KorisnikDTO {
		private Long id;

	    @NotBlank
	    private String korisnickoIme;
	    
	    private String datumRegistracije;
	    
	    

	   

	    

	    public String getDatumRegistracije() {
			return datumRegistracije;
		}

		public void setDatumRegistracije(String datumRegistracije) {
			this.datumRegistracije = datumRegistracije;
		}

		public Long getId() {
	        return id;
	    }

	    public void setId(Long id) {
	        this.id = id;
	    }

	    public String getKorisnickoIme() {
	        return korisnickoIme;
	    }

	    public void setKorisnickoIme(String korisnickoIme) {
	        this.korisnickoIme = korisnickoIme;
	    }


}
