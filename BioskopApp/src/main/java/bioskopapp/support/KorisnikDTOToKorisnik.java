package bioskopapp.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bioskopapp.model.Korisnik;
import bioskopapp.service.KorisnikService;
import bioskopapp.web.dto.KorisnikDTO;

@Component
public class KorisnikDTOToKorisnik implements Converter<KorisnikDTO,Korisnik>{
	
	@Autowired
	private KorisnikService korisnikService;

	@Override
	public Korisnik convert(KorisnikDTO dto) {
		Korisnik korisnik = null;
		
		 if(dto.getId() != null) {
	            korisnik = korisnikService.findOne(dto.getId()).get();
	        }

	        if(korisnik == null) {
	            korisnik = new Korisnik();
	        }
		
	        korisnik.setKorisnickoIme(dto.getKorisnickoIme());
	        korisnik.setDatumRegistracije(getLocalDate(dto.getDatumRegistracije()));
		return korisnik;
	}
	
	private LocalDate getLocalDate(String datumS) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate datum = LocalDate.parse(datumS, formatter);
     
        return datum;
    }

}
