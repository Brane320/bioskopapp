package bioskopapp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bioskopapp.model.Film;
import bioskopapp.web.dto.FilmDTO;

@Component
public class FilmToFilmDTO implements Converter<Film,FilmDTO>{

	@Override
	public FilmDTO convert(Film source) {
		FilmDTO dto = new FilmDTO();
		
		dto.setId(source.getId());
		dto.setDistributer(source.getDistributer());
		dto.setGlumci(source.getGlumci());
		dto.setGodinaProizvodnje(source.getGodinaProizvodnje());
		dto.setNaziv(source.getNaziv());
		dto.setOpis(source.getOpis());
		dto.setReziser(source.getReziser());
		dto.setTrajanje(source.getTrajanje());
		dto.setZanrovi(source.getZanrovi());
		dto.setZemljaPorekla(source.getZemljaPorekla());
		
		return dto;
	}
	
	public List<FilmDTO> convert(List<Film> filmovi){
		List<FilmDTO> dto = new ArrayList<>();
		
		for(Film film : filmovi) {
			dto.add(convert(film));
		}
		return dto;
	}

}
