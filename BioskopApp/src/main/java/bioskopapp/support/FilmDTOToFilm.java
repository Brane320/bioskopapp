package bioskopapp.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bioskopapp.model.Film;
import bioskopapp.service.FilmService;
import bioskopapp.web.dto.FilmDTO;

@Component
public class FilmDTOToFilm implements Converter<FilmDTO,Film> {
	@Autowired
	private FilmService filmService;

	@Override
	public Film convert(FilmDTO dto) {
		Film film;
		
		if(dto.getId() == null) {
			film = new Film();
		} else {
			film = filmService.getOne(dto.getId());
		}
		
		if(film != null) {
			film.setDistributer(dto.getDistributer());
			film.setGlumci(dto.getGlumci());
			film.setGodinaProizvodnje(dto.getGodinaProizvodnje());
			film.setNaziv(dto.getNaziv());
			film.setOpis(dto.getOpis());
			film.setReziser(dto.getReziser());
			film.setTrajanje(dto.getTrajanje());
			film.setZanrovi(dto.getZanrovi());
			film.setZemljaPorekla(dto.getZemljaPorekla());
		}
		return film;
		
	}

}
