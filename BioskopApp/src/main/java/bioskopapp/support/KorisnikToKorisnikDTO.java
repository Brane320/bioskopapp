package bioskopapp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bioskopapp.model.Korisnik;
import bioskopapp.web.dto.KorisnikDTO;

@Component
public class KorisnikToKorisnikDTO implements Converter<Korisnik,KorisnikDTO> {

	@Override
	public KorisnikDTO convert(Korisnik source) {
		KorisnikDTO dto = new KorisnikDTO();
		
		dto.setId(source.getId());
		dto.setKorisnickoIme(source.getKorisnickoIme());
		dto.setDatumRegistracije(source.getDatumRegistracije().toString());
		
		return dto;
	}
	
	public List<KorisnikDTO> convert(List<Korisnik> korisnici){
		 List<KorisnikDTO> korisnikDTOS = new ArrayList<>();

	        for(Korisnik k : korisnici) {
	            KorisnikDTO dto = convert(k);
	            korisnikDTOS.add(dto);
	        }

	        return korisnikDTOS;
	}

}
