package bioskopapp.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import bioskopapp.model.Film;

public interface FilmService {
	
	Page<Film> findAll(int brojStranice);
	
	Film getOne(Long id);
	
	Optional<Film> findById(Long id);
	
	Film save(Film film);
	
	Film delete(Long id);
	
	Film update(Film film);

}
