package bioskopapp.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import bioskopapp.model.Film;
import bioskopapp.repository.FilmRepository;
import bioskopapp.service.FilmService;

@Service
public class JpaFilmService implements FilmService {
	
	@Autowired
	private FilmRepository filmRepository;

	@Override
	public Page<Film> findAll(int brojStranice) {
		return filmRepository.findAll(PageRequest.of(brojStranice, 3));
	}

	@Override
	public Film getOne(Long id) {
		return filmRepository.getOne(id);
	}

	@Override
	public Optional<Film> findById(Long id) {
		return filmRepository.findById(id);
	}

	@Override
	public Film save(Film film) {
		return filmRepository.save(film);
	}

	@Override
	public Film delete(Long id) {
		Optional<Film> film = filmRepository.findById(id);
		
		if(film.isPresent()) {
			filmRepository.deleteById(id);
			return film.get();
		}
		return null;
	}

	@Override
	public Film update(Film film) {
		return filmRepository.save(film);
	}

}
