package bioskopapp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Sala {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false)
	private String naziv;
	
	@ManyToMany
	private List<TipProjekcije> tipoviProjekcija;
	
	@OneToMany(mappedBy = "sala", fetch = FetchType.EAGER)
	private List<Sediste> sedista;
	
	
	public List<Sediste> getSedista() {
		return sedista;
	}
	public void setSedista(List<Sediste> sedista) {
		this.sedista = sedista;
	}
	public Sala() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public List<TipProjekcije> getTipoviProjekcija() {
		return tipoviProjekcija;
	}
	public void setTipoviProjekcija(List<TipProjekcije> tipoviProjekcija) {
		this.tipoviProjekcija = tipoviProjekcija;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sala other = (Sala) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
