package bioskopapp.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import bioskopapp.enumeration.KorisnickaUloga;





@Entity
public class Korisnik {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
    @Column(unique = true, nullable = false)
    private String korisnickoIme;
    
    @Column(unique = true, nullable = false)
    private String lozinka;

   @Column
   private LocalDate datumRegistracije;

    @Enumerated(EnumType.STRING)
    private KorisnickaUloga uloga;

   

    public Korisnik(){

    }

    

    public Korisnik(long id, String korisnickoIme, String lozinka, LocalDate datumRegistracije, KorisnickaUloga uloga) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.datumRegistracije = datumRegistracije;
		this.uloga = uloga;
	}


    
    
    
    


    

    public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getKorisnickoIme() {
		return korisnickoIme;
	}



	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}



	public String getLozinka() {
		return lozinka;
	}



	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}



	public LocalDate getDatumRegistracije() {
		return datumRegistracije;
	}



	public void setDatumRegistracije(LocalDate datumRegistracije) {
		this.datumRegistracije = datumRegistracije;
	}



	public KorisnickaUloga getUloga() {
		return uloga;
	}



	public void setUloga(KorisnickaUloga uloga) {
		this.uloga = uloga;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Korisnik other = (Korisnik) obj;
		if (id != other.id)
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Korisnik [id=" + id + ", korisnickoIme=" + korisnickoIme + ", lozinka=" + lozinka
				+ ", datumRegistracije=" + datumRegistracije + ", uloga=" + uloga + "]";
	}

	

}
