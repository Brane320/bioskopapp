

INSERT INTO korisnik (id, datum_registracije, korisnicko_ime, lozinka,uloga)
              VALUES (1,'2020-11-15','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','ADMIN');
INSERT INTO korisnik (id, datum_registracije, korisnicko_ime, lozinka, uloga)
              VALUES (2,'2020-11-15','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','KORISNIK');
INSERT INTO korisnik (id, datum_registracije, korisnicko_ime, lozinka, uloga)
              VALUES (3,'2020-11-15','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','KORISNIK');
              
INSERT INTO tip_projekcije (id,naziv) VALUES (1,'2D');
INSERT INTO tip_projekcije (id,naziv) VALUES (2,'3D');
INSERT INTO tip_projekcije (id,naziv) VALUES (3,'4D');

INSERT INTO sala (id,naziv) VALUES (1,'Sala 1');
INSERT INTO sala (id,naziv) VALUES (2,'Sala 2');
INSERT INTO sala (id,naziv) VALUES (3,'Sala 3');
INSERT INTO sala (id,naziv) VALUES (4,'Sala 4');

INSERT INTO sala_tipovi_projekcija (sala_id,tipovi_projekcija_id) VALUES (1,2);
INSERT INTO sala_tipovi_projekcija (sala_id,tipovi_projekcija_id) VALUES (1,3);
INSERT INTO sala_tipovi_projekcija (sala_id,tipovi_projekcija_id) VALUES (2,1);
INSERT INTO sala_tipovi_projekcija (sala_id,tipovi_projekcija_id) VALUES (2,2);
INSERT INTO sala_tipovi_projekcija (sala_id,tipovi_projekcija_id) VALUES (3,1);
INSERT INTO sala_tipovi_projekcija (sala_id,tipovi_projekcija_id) VALUES (4,1);
INSERT INTO sala_tipovi_projekcija (sala_id,tipovi_projekcija_id) VALUES (4,2);
INSERT INTO sala_tipovi_projekcija (sala_id,tipovi_projekcija_id) VALUES (4,3);

INSERT INTO sediste (redni_broj,sala_id) VALUES (1,1);
INSERT INTO sediste (redni_broj,sala_id) VALUES (2,1);
INSERT INTO sediste (redni_broj,sala_id) VALUES (3,1);
INSERT INTO sediste (redni_broj,sala_id) VALUES (4,1);
INSERT INTO sediste (redni_broj,sala_id) VALUES (5,1);
INSERT INTO sediste (redni_broj,sala_id) VALUES (6,1);

INSERT INTO sediste (redni_broj,sala_id) VALUES (1,2);
INSERT INTO sediste (redni_broj,sala_id) VALUES (2,2);
INSERT INTO sediste (redni_broj,sala_id) VALUES (3,2);
INSERT INTO sediste (redni_broj,sala_id) VALUES (4,2);
INSERT INTO sediste (redni_broj,sala_id) VALUES (5,2);
INSERT INTO sediste (redni_broj,sala_id) VALUES (6,2);

INSERT INTO sediste (redni_broj,sala_id) VALUES (1,3);
INSERT INTO sediste (redni_broj,sala_id) VALUES (2,3);
INSERT INTO sediste (redni_broj,sala_id) VALUES (3,3);
INSERT INTO sediste (redni_broj,sala_id) VALUES (4,3);
INSERT INTO sediste (redni_broj,sala_id) VALUES (5,3);
INSERT INTO sediste (redni_broj,sala_id) VALUES (6,3);
INSERT INTO sediste (redni_broj,sala_id) VALUES (7,3);
INSERT INTO sediste (redni_broj,sala_id) VALUES (8,3);

INSERT INTO sediste (redni_broj,sala_id) VALUES (1,4);
INSERT INTO sediste (redni_broj,sala_id) VALUES (2,4);
INSERT INTO sediste (redni_broj,sala_id) VALUES (3,4);
INSERT INTO sediste (redni_broj,sala_id) VALUES (4,4);
INSERT INTO sediste (redni_broj,sala_id) VALUES (5,4);
INSERT INTO sediste (redni_broj,sala_id) VALUES (6,4);

INSERT INTO film (id,distributer,glumci,godina_proizvodnje,naziv,opis,reziser,trajanje,zanrovi,zemlja_porekla) 
VALUES (1,'Hollywood',' Robert De Niro, Sharon Stone, Joe Pesci', 1995, 'Casino','A tale of greed, deception, money, power, and murder occur between two best friends: a mafia enforcer and a casino executive compete against each other over a gambling empire, and over a fast-living and fast-loving socialite.', ' Martin Scorsese', 165,'Drama','USA');
INSERT INTO film (id,distributer,glumci,godina_proizvodnje,naziv,opis,reziser,trajanje,zanrovi,zemlja_porekla) 
VALUES (2,'Hollywood',' Arnold Schwarzenegger, Linda Hamilton, Michael Biehn', 1984, 'Terminator','A human soldier is sent from 2029 to 1984 to stop an almost indestructible cyborg killing machine, sent from the same year, which has been programmed to execute a young woman whose unborn son is the key to humanity future salvation.', 'James Cameron', 107,'Akcija','USA');
INSERT INTO film (id,distributer,glumci,godina_proizvodnje,naziv,opis,reziser,trajanje,zanrovi,zemlja_porekla) 
VALUES (3,'Hollywood',' Leonardo DiCaprio, Jonah Hill, Margot Robbie', 2013, 'The Wolf Of Wall Street','Based on the true story of Jordan Belfort, from his rise to a wealthy stock-broker living the high life to his fall involving crime, corruption and the federal government.', 'Martin Scorsese', 180,'Biografija,Drama','USA');

              

              
